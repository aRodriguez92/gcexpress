class WelcomeController < ApplicationController
  layout 'blank_main', only: [:blank]
  def index
    redirect_to intel_intel_analytics_dashboard_path
  end

  def blank
  end
end
