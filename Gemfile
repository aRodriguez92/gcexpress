source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.1'

gem 'audited'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'coffee-rails'
gem 'devise'
gem 'enum_help'
gem 'friendly_id'
gem 'honeybadger'
gem 'jbuilder', '~> 2.7'
gem 'local_time'
gem 'name_of_person'
gem 'noticed'
gem 'paranoia'
gem 'pg'
gem 'puma'
gem 'rails', '6.1.4'
gem 'roo'
gem 'roo-xls'
gem 'sass-rails', '>= 6'
gem 'simple_form'
gem 'slim-rails'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'webpacker', '~> 4.0'

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'ffaker'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'listen'
  gem 'rack-mini-profiler'
  gem 'rubocop'
  gem 'rubocop-gitlab-security'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'spring'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'fuubar'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'simplecov', '~> 0.21.2', require: false
  gem 'webdrivers'
end
